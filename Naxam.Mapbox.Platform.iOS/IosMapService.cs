﻿using System;
using Foundation;
using Mapbox;
using ObjCRuntime;

namespace Naxam.Mapbox.Platform.iOS
{
    public class IosMapService
    {
        public IosMapService()
        {
        }

        public void DownloadOfflineMap(string style) {
            System.Diagnostics.Debug.WriteLine("bingo1");

            // Setup offline pack notification handlers.
            NSNotificationCenter.DefaultCenter.AddObserver(MGLOfflinePackKeys.ProgressChangedNotification, OfflinePackProgressDidChange);
            NSNotificationCenter.DefaultCenter.AddObserver(MGLOfflinePackKeys.ErrorNotification, OfflinePackDidReceiveError);
            //NSNotificationCenter.DefaultCenter.AddObserver(this, new Selector(nameof(DownloadDelegates.OfflinePackProgressDidChange)), nameof(MGLOfflinePackProgressChanged), null);
            //NSNotificationCenter.DefaultCenter.AddObserver(self, selector: #selector(offlinePackDidReceiveError), name: NSNotification.Name.MGLOfflinePackError, object: nil)
            //NSNotificationCenter.DefaultCenter.AddObserver(self, selector: #selector(offlinePackDidReceiveMaximumAllowedMapboxTiles), name: NSNotification.Name.MGLOfflinePackMaximumMapboxTilesReached, object: nil)
            System.Diagnostics.Debug.WriteLine("bingo2");

            this.StartOfflinePackDownload(style, 6, 10);

            // Remove offline pack observers.
            //NSNotificationCenter.DefaultCenter.RemoveObserver();

        }

        public void StartOfflinePackDownload(string styleUrl, int minZoom, int maxZoom)
        {
            System.Diagnostics.Debug.WriteLine("bingo3");
            var bounds = new MGLCoordinateBounds();
            bounds.ne = new CoreLocation.CLLocationCoordinate2D(49, -92);
            bounds.sw = new CoreLocation.CLLocationCoordinate2D(43, -95);

            // Create a region that includes the current viewport and any tiles needed to view it when zoomed further in.
            // Because tile count grows exponentially with the maximum zoom level, you should be conservative with your `toZoomLevel` setting.
            MGLTilePyramidOfflineRegion region = new MGLTilePyramidOfflineRegion(new NSUrl(styleUrl), bounds, minZoom, maxZoom);
            //var region = new MGLOfflineRegion()new NSUrl(styleUrl), bounds, minZoom, maxZoom);


            // Store some data for identification purposes alongside the downloaded resources.
            NSObject userInfo = new NSDictionary("name", "My Offline Pack");
            var context = NSKeyedArchiver.ArchivedDataWithRootObject(userInfo);
            //var region2 = (NSObject)region;

            //MGLOfflineRegion region3 = region2 as MGLOfflineRegion;



            // Create and register an offline pack with the shared offline storage object.
            MGLOfflineStorage.SharedOfflineStorage().AddPackForRegion(
                (MGLOfflineRegion)region,
                context,
                (pack, e) =>
                {
                    System.Diagnostics.Debug.WriteLine("Pack: " + pack.Progress.countOfResourcesExpected);
                    if (e != null)
                    {
                        System.Diagnostics.Debug.WriteLine("Error: " + e.LocalizedDescription ?? "unknown error");
                    }
                    else
                    {
                        pack.Resume();
                    }
                });
        }

        //private class DownloadDelegates : NSObject 
        //{
            public void OfflinePackProgressDidChange(NSNotification notification)
            {
                // Get the offline pack this notification is regarding,
                // and the associated user info for the pack; in this case, `name = My Offline Pack`
                var pack = notification.Object as MGLOfflinePack;
                var userInfo = NSKeyedUnarchiver.UnarchiveObject(pack.Context) as NSDictionary;
                if (pack != null && userInfo != null)
                {
                    var progress = pack.Progress;
                    // or notification.userInfo![MGLOfflinePackProgressUserInfoKey]!.MGLOfflinePackProgressValue
                    var completedResources = progress.countOfResourcesCompleted;
                    var expectedResources = progress.countOfResourcesExpected;

                    // Calculate current progress percentage.
                    var progressPercentage = (float)completedResources / expectedResources;

                    // If this pack has finished, print its size and resource count.
                    //if (completedResources == expectedResources) {
                    //var byteCount = ByteCountFormatter.string(fromByteCount: (long)pack.Progress.countOfBytesCompleted, countStyle: ByteCountFormatter.CountStyle.memory)
                    //System.Console.WriteLine("Offline pack '" + userInfo["name"] ?? "unknown" + "' completed: " + byteCount + " of " + completedResources + " resources");
                    //}
                    System.Diagnostics.Debug.WriteLine("Offline pack '" + userInfo["name"] ?? "unknown" + "' has " + completedResources + " of " + expectedResources + " resources — " + progressPercentage * 100 + "%.");
                }
            }

            public void OfflinePackDidReceiveError(NSNotification notification)
            {
                var pack = notification.Object as MGLOfflinePack;
                var userInfo = NSKeyedUnarchiver.UnarchiveObject(pack.Context) as NSDictionary;
                NSObject error = null;
                if (pack != null && userInfo != null && notification.UserInfo.TryGetValue((NSString)"error", out error))
                {
                    System.Diagnostics.Debug.WriteLine("Offline pack '" + userInfo["name"] ?? "unknown" + "' reveived error: " + error);
                }
            }
    //    }

        //@objc func offlinePackDidReceiveMaximumAllowedMapboxTiles(notification: NSNotification)
        //{
        //    if let pack = notification.object as? MGLOfflinePack,
        //    let userInfo = NSKeyedUnarchiver.unarchiveObject(with: pack.context) as?[String: String],
        //    let maximumCount = (notification.userInfo?[MGLOfflinePackUserInfoKey.maximumCount] as AnyObject).uint64Value {
        //        print("Offline pack “\(userInfo["name"] ?? "unknown")” reached limit of \(maximumCount) tiles.")
        //}
        //}

    }
}
