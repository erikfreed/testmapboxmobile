﻿using Foundation;
using UIKit;
using Naxam.Controls;
using Naxam.Controls.Forms;
using System;
using System.Collections.Generic;
using System.Linq;
using CoreGraphics;
using Mapbox;
using Xamarin.Forms.Platform.iOS;
using CoreLocation;
using FormsMap = Naxam.Controls.Forms.MapView;
using FormsMB = Naxam.Controls.Forms;
using System.Collections.Specialized;
using Xamarin.Forms;
using Naxam.Mapbox.Platform.iOS;

namespace MapBoxQs.iOS
{
    [Register("AppDelegate")]
    public partial class AppDelegate : global::Xamarin.Forms.Platform.iOS.FormsApplicationDelegate
    {
        public override bool FinishedLaunching(UIApplication app, NSDictionary options)
		{
            MGLAccountManager.AccessToken = MapBoxQs.Services.MapBoxService.AccessToken;
            new Naxam.Controls.Platform.iOS.MapViewRenderer();

            global::Xamarin.Forms.Forms.Init();

            LoadApplication(new App());

            return base.FinishedLaunching(app, options);
        }

        //private void DownloadOfflineMap() {
        //    var region = MGLTilePyramidOfflineRegion(styleURL: mapView.styleURL, bounds: mapView.visibleCoordinateBounds, fromZoomLevel: mapView.zoomLevel, toZoomLevel: 16)    
        //}

    }
}
